<div class="footer">
    <div class="container">
        <div class="row">
            <?php
            global $bestel_opt;
			if (!empty($bestel_opt['bestel-footer-catelog']) && ($bestel_opt['bestel-footer-catelog'] == 1)) {
            	do_action('bestel_footer_catelog');
			}
            ?>
            <div class="col-lg-7 ml-lg-auto d-flex flex-column position-static">
                <div class="row">
                    <div class="col-sm col-lg-5">
                        <?php
                        if (is_active_sidebar('footer_our_sevices')) {
                            dynamic_sidebar('footer_our_sevices');
                        }
                        ?>
                    </div>
                    <?php if (!empty($bestel_opt['bestel-footer-contact-info']) && ($bestel_opt['bestel-footer-contact-info'] == 1)) { ?>
                        <div class="col-sm mt-2 mt-sm-0">
                            <h4>
                                <?php
                                if (!empty($bestel_opt['bestel-page-footer-title']) && ($bestel_opt['bestel-page-footer-title'] != '')) {
                                    echo wp_kses_post($bestel_opt['bestel-page-footer-title']);
                                }
                                ?>
                            </h4>
                            <ul class="icn-list">
                                <?php if (!empty($bestel_opt['bestel-phone-caption']) && ($bestel_opt['bestel-phone-caption'] != '')) { ?>
                                    <li> <i class="icon-placeholder"></i><?php echo wp_kses_post($bestel_opt['bestel-phone-caption']); ?></li>  
                                <?php } ?>
                                <?php if (!empty($bestel_opt['bestel-phone-number']) && ($bestel_opt['bestel-phone-number'] != '')) { ?>
                                    <li><i class="icon-telephone"></i><span class="phone"><?php echo wp_kses_post($bestel_opt['bestel-phone-number']); ?> </span></li>
                                <?php } ?>
                                <?php if (!empty($bestel_opt['bestel-page-email-text']) && ($bestel_opt['bestel-page-email-text'] != '')) { ?>
                                    <li><i class="icon-closed-envelope"></i><a href="mailto:<?php echo wp_kses_post($bestel_opt['bestel-page-email-text']); ?>"><?php echo wp_kses_post($bestel_opt['bestel-page-email-text']); ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
                <div class="row mt-5 mb-1 mt-lg-auto mb-md-0 align-items-center">
                    <div class="col-sm-auto col-lg-12 col-xl-auto">
                        <div class="footer-copyright text-left">
                            <?php
                                if (!empty($bestel_opt['bestel-footer_copyright']) && ($bestel_opt['bestel-footer_copyright'] != '')) {
                                    echo wp_kses_post($bestel_opt['bestel-footer_copyright']);
                                }
                            ?>
                            <div class="clearfix d-md-none"></div>
                            <?php
                                if (!empty($bestel_opt['bestel_footer_term_condition_enable']) && ($bestel_opt['bestel_footer_term_condition_enable'] == 1)){
                                    if (!empty($bestel_opt['bestel_footer_copyright_conditions']) && ($bestel_opt['bestel_footer_copyright_conditions'] != '')) {
                                        echo wp_kses_post($bestel_opt['bestel_footer_copyright_conditions']);
                                    }
                                }
                            ?>
                        </div>
                    </div>
                    <?php if (!empty($bestel_opt['bestel-footer-social-info']) && ($bestel_opt['bestel-footer-social-info'] == 1)) { ?>
                    <div class="col-sm mt-2 mt-sm-0 mt-md-1 mt-xl-0">
                        <div class="footer-social text-left text-sm-right text-lg-left text-xl-right">
                            <?php if (!empty($bestel_opt['bestel-footer-facebook']) && !empty($bestel_opt['bestel-footer-facebook'])) { ?>
                                <a target="_blank" href="<?php echo esc_url($bestel_opt['bestel-footer-facebook']); ?>"><i class="icon-facebook-logo"></i></a>
                            <?php } ?>
                            <?php if (!empty($bestel_opt['bestel-footer-twitter']) && !empty($bestel_opt['bestel-footer-twitter'])) { ?>
                                <a target="_blank" href="<?php echo esc_url($bestel_opt['bestel-footer-twitter']); ?>"><i class="icon-twitter-logo1"></i></a>
                            <?php } ?>
                            <?php if (!empty($bestel_opt['bestel-footer-google-plus']) && !empty($bestel_opt['bestel-footer-google-plus'])) { ?>
                                <a target="_blank" href="<?php echo esc_url($bestel_opt['bestel-footer-google-plus']); ?>"><i class="icon-google-logo"></i></a>
                            <?php } ?>
                            <?php if (!empty($bestel_opt['bestel-footer-instagram']) && !empty($bestel_opt['bestel-footer-instagram'])) { ?>
                                <a target="_blank" href="<?php echo esc_url($bestel_opt['bestel-footer-instagram']); ?>"><i class="icon-instagram-logo"></i></a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php do_action('bestel_after_footer'); ?>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($bestel_opt['bestel-footer-backTotop']) && ($bestel_opt['bestel-footer-backTotop'] == 1)) { ?>
    <div class="backToTop js-backToTop">
        <i class="icon icon-up-arrow"></i>
    </div>
<?php } ?>

<?php
do_action('reservation_popup');
do_action('resturent_reservation_popup');
?>
<?php wp_footer(); ?>
<script src="https://widget.siteminder.com/ibe.min.js"></script>
</body>
</html>