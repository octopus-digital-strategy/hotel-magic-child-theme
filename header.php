<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="format-detection" content="telephone=no"> 
        <?php
        global $bestel_opt;
        $fevicon = $bestel_opt['bestel-site-favicon'];
        if (isset($fevicon['url']) && !empty($fevicon['url'])) {
            ?>
            <link rel="shortcut icon" href="<?php echo esc_url($fevicon['url']) ?>" type="image/x-icon"/>
            <?php
        }
        ?>
<?php wp_head(); ?>
    </head>
    <body  <?php body_class(); ?>>  
        <header class="header">
            <?php if (!empty($bestel_opt['bestel-page-header-mobile']) && $bestel_opt['bestel-page-header-mobile'] == 1) { ?>
                <div class="header-mobile-info">
                    <div class="header-mobile-slide info1">
                        <div class="inside">
                            <div class="row">
                                <div class="col-sm">
                                    <?php if (!empty($bestel_opt['bestel-page-header-mobile-skype']) && $bestel_opt['bestel-page-header-mobile-skype'] != '') { ?>
                                        <p><i class="icon-skype-logo"></i><a href="skype:<?php echo esc_url($bestel_opt['bestel-page-header-mobile-skype']); ?>?chat"><?php echo wp_kses_post($bestel_opt['bestel-page-header-mobile-skype']); ?></a></p>
                                    <?php }
                                    ?>
                                    <?php if (!empty($bestel_opt['bestel-page-header-mobile-viber']) && $bestel_opt['bestel-page-header-mobile-viber'] != '') { ?>
                                        <p><i class="icon-viber"></i><a href="viber://chat?number=<?php echo esc_url($bestel_opt['bestel-page-header-mobile-viber']); ?>"><?php echo wp_kses_post($bestel_opt['bestel-page-header-mobile-viber']); ?></a></p>
                                    <?php }
                                    ?>
                                </div>
                                <div class="col-sm mt-15 mt-sm-0">
                                    <?php if (!empty($bestel_opt['bestel-page-header-mobile-telegram']) && $bestel_opt['bestel-page-header-mobile-telegram'] != '') { ?>
                                        <p><i class="icon-telegram"></i><a href="tg://resolve?domain=<?php echo esc_url($bestel_opt['bestel-page-header-mobile-telegram']); ?>"><?php echo wp_kses_post($bestel_opt['bestel-page-header-mobile-telegram']); ?></a></p>
                                    <?php }
                                    ?>

                                    <?php if (!empty($bestel_opt['bestel-page-header-mobile-email']) && $bestel_opt['bestel-page-header-mobile-email'] != '') { ?>
                                        <p><i class="icon-closed-envelope"></i><a href="mailto:<?php echo bestel_get_option('bestel-page-header-mobile-email'); ?>"><?php echo wp_kses_post(bestel_get_option('bestel-page-header-mobile-email')); ?></a></p>

                                    <?php }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-mobile-slide info2">
                        <div id="googleMapHeader" class="google-map"></div>
                    </div>
                    <div class="header-mobile-slide info3">
                        <div class="inside">
                            <?php echo get_template_part('searchform', 'top-mobile'); ?>
                        </div>
                    </div>
                    <div class="header-mobile-slide info4">
                        <?php echo do_action('lang_mobile_slide_inc'); ?>
                    </div>
                </div>
            <?php } ?>
            <?php if (($bestel_opt['bestel-page-header-topline'] != '')) { ?>
                <div class="header-topline">
                    <div class="container">
                        <div class="row align-items-center">
                            <?php
                            do_action('weather_and_local_time');
                            ?>
                            <div class="col-auto ml-auto d-none d-lg-flex">
                                <div class="header-search">
                                    <?php echo get_template_part('searchform', 'top'); ?>
                                </div>
                                <div class="header-lang">
                                    <?php echo do_action('lang_inc'); ?>
                                </div>
                            </div>
                            <div class="col col-md-auto ml-auto justify-content-end d-flex d-lg-none">
                                <?php echo do_action('mobile_lang_inc'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="header-content">
                <div class="container">
                    <div class="row">
                        <div class="col-auto col-sm-5 col-lg-3 d-flex align-items-center bestel-logo">
                            <?php
                            $bestel_logo = $bestel_opt['bestel-logo'];
                            ?>
                            <a href="<?php echo esc_url(home_url('/')) ?>" class="header-logo"><img src="<?php echo esc_url($bestel_logo['url']) ?>" alt="<?php echo esc_attr('logo') ?> " class="img-fluid"></a>
                        </div>
                        <div class="col col-md-auto col-lg-3 d-flex">
                            <?php
                            if (!empty($bestel_opt['bestel-page-header-widget-booking']) && $bestel_opt['bestel-page-header-widget-booking'] == 1) {
                                do_action('bestel_header_catelog');
                            }
                            ?>
                        </div>
                        <?php if (!empty($bestel_opt['bestel-page-header-contact']) && $bestel_opt['bestel-page-header-contact'] == 1) { ?>

                            <div class="col-sm-3 d-none d-lg-block">
                                <div class="header-contact d-flex">
                                    <div class="header-contact-icon"><i class="icon-placeholder"></i></div>
                                    <div class="header-contact-txt">
                                        <p>
                                            <?php
                                            if (!empty($bestel_opt['bestel-page-header-location-1']) && $bestel_opt['bestel-page-header-location-1'] != '') {
                                                echo wp_kses_post($bestel_opt['bestel-page-header-location-1']);
                                            }
                                            ?>
                                            <?php if (!empty($bestel_opt['bestel-page-header-location-2']) && $bestel_opt['bestel-page-header-location-2'] != '') { ?>
                                            <p class="txt-sm">
                                                <?php echo wp_kses_post($bestel_opt['bestel-page-header-location-2']); ?>
                                            </p>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3 d-none d-lg-block">
                                <div class="header-contact d-flex">
                                    <div class="header-contact-icon"><i class="icon-telephone"></i></div>
                                    <div class="header-contact-txt">
                                        <p class="txt-lg">
                                            <?php
                                            if (!empty($bestel_opt['bestel-page-header-phone']) && $bestel_opt['bestel-page-header-phone'] != '') {
                                                echo wp_kses_post($bestel_opt['bestel-page-header-phone']);
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="header-contact d-flex">
                                    <div class="header-contact-icon"><i class="icon-closed-envelope"></i></div>
                                    <div class="header-contact-txt">
                                        <a href="mailto:<?php echo bestel_get_option('bestel-page-header-email') ?>"> 
                                            <?php
                                            if (!empty($bestel_opt['bestel-page-header-email']) && $bestel_opt['bestel-page-header-email'] != '') {
                                                echo wp_kses_post($bestel_opt['bestel-page-header-email']);
                                            }
                                            ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-auto ml-auto d-flex d-lg-none">
                            <button class="navbar-btn" data-target='#modalNavigation' data-toggle='modal'>
                                <i class="icon-menu"></i>
                            </button>
                            <div class='modal fade modal-fullscreen-menu' id='modalNavigation'>
                                <button aria-label='Close' class='close' data-dismiss='modal'>
                                    <i class="icon-close"></i><?php echo esc_html__('CLOSE', 'bestel') ?>
                                </button>
                                <div class='modal-dialog'>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $divClass = '';
            if ($bestel_opt['bestel_is_sticky_header'] != NULL && $bestel_opt['bestel_is_sticky_header']):
                $divClass = 'sticky';
            endif;
            ?>
            <div class="header-nav js-header-nav <?php echo esc_attr($divClass); ?>">
                <div class="container">
                    <nav class="navbar navbar-expand-lg">
                        <div class="navbar-collapse">
                            <?php
                            wp_nav_menu(
                                    array(
                                        'theme_location' => 'primary',
                                        'menu_class' => 'menu navbar-nav w-100 js-main-menu',
                                        'container' => 'ul',
                                        'walker' => new Walker_Best_Hotel_Menu()
                                    )
                            );
                            ?>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        
